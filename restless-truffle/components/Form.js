import * as React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { TextInput, Button } from 'react-native-paper';

export default function Form() {
  return (
    <View style={[styles.container, { flexDirection: 'column' }]}>
      <View style={[styles.header, { flex: 1 }]}>
        <Text style={[styles.title]}>Welcome</Text>
      </View>

      <View style={[{ flex: 4 }]}>
        <TextInput
          style={[styles.input, { marginTop: 15 }]}
          selectionColor={[COLORS.backgroundColor]}
          underlineColor={COLORS.backgroundColor}
          label="email"
          theme={{ colors: { primary: COLORS.backgroundColor } }}
        />

        <TextInput
          style={styles.input}
          selectionColor={[COLORS.backgroundColor]}
          underlineColor={COLORS.backgroundColor}
          label="Password"
          theme={{ colors: { primary: COLORS.backgroundColor } }}
        />

        <Button mode="flat" color={[COLORS.backgroundColor]}>
          Se Connecter
        </Button>
      </View>
    </View>
  );
}

export const COLORS = {
  backgroundColor: '#01DFD7',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20,
  },
  input: {
    marginBottom: 15,
  },
  title: {
    fontSize: 40,
    color: 'white',
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlignVertical: true,
    backgroundColor: COLORS.backgroundColor,
  },
});
